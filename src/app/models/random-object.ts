export class RandomObject {
  type: string;
  format: string;
  number: number;
  number_max: number;
  amount: number;
  time: string;
  text_out: string;
  componentName: string;
  textWordCount: number;
}
