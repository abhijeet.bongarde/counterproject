import {Component, Input, OnInit} from '@angular/core';
import {RandomObjectService} from '../../services/random-object.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {validateAllFormFields} from '../../common/utils';

@Component({
  selector: 'app-random-text1',
  templateUrl: './random-text.component.html',
  styleUrls: ['./random-text.component.css']
})
export class RandomTextComponent implements OnInit {
  @Input() componentName: string;
  remainingTextExperience = 200;
  randomTextForm: FormGroup = new FormGroup({
    urlName: new FormControl('',
      [Validators.required,
        Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]),
    textWordCount: new FormControl(0)
  });

  constructor(private randomObjectService: RandomObjectService) {
  }

  ngOnInit(): void {
    this.randomTextForm.controls.urlName.valueChanges.subscribe(value => {
      const length = value ? value.length : 0;
      this.remainingTextExperience = length < 200 ? 200 - length : 0;
    });
    this.randomObjectService.currentData.subscribe(res => {
       res.forEach(data => {
         if (data.componentName === this.componentName) {
           this.randomTextForm.patchValue({
             textWordCount: data.textWordCount
           });
         }
       });
    });
  }

  get f(): any {
    return this.randomTextForm.controls;
  }

  fetchData(): void {
    if (!this.randomTextForm.valid) {
      validateAllFormFields(this.randomTextForm);
      return;
    } else {
      this.randomObjectService.getRandomObject(this.componentName, this.randomTextForm.value.urlName);
    }
  }
}
