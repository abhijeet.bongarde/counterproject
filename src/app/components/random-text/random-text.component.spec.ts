import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomTextComponent } from './random-text.component';
import {HttpClientModule} from "@angular/common/http";

describe('RandomText1Component', () => {
  let component: RandomTextComponent;
  let fixture: ComponentFixture<RandomTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RandomTextComponent ],
      imports: [HttpClientModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
