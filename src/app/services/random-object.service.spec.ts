import {async, TestBed} from '@angular/core/testing';

import {RandomObjectService} from './random-object.service';
import {HttpClientModule} from '@angular/common/http';

describe('RandomObjectService', () => {
  let service: RandomObjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(RandomObjectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should generate random object', async(() => {
    service.getRandomObject('comp1', 'http://www.randomtext.me/api/');
    // @ts-ignore
    service.currentData._subscribe((res: any) => {
      expect(res).not.toBeNull();
    });
  }));
});
