import {Injectable} from '@angular/core';
import {RandomObject} from '../models/random-object';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RandomObjectService {
  private randomObject: RandomObject[] = [];
  dataSubject: BehaviorSubject<RandomObject[]> = new BehaviorSubject<RandomObject[]>(this.randomObject);
  currentData: Observable<RandomObject[]> = this.dataSubject.asObservable();

  constructor(private httpClient: HttpClient) {
  }

  getRandomObject(componentName: string, url: string): void {
    this.httpClient.get(url).subscribe(res => {
      const newObj: RandomObject = res as RandomObject;
      newObj.componentName = componentName;
      newObj.textWordCount = newObj.text_out.split(' ').length;
      this.onChangeData(newObj);
    });
  }

  onChangeData(newData: RandomObject): void {
    this.randomObject.push(newData);
    this.dataSubject.next(this.randomObject);
  }
}
