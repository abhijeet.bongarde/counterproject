import {FormControl, FormGroup} from '@angular/forms';

export function validateAllFormFields(formGroup: FormGroup): any {
  formGroup.markAllAsTouched();
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({onlySelf: true});
    } else if (control instanceof FormGroup) {
      validateAllFormFields(control);
    }
  });
}
